export class Customer {
    perso!: {
      firstName: string,
      lastName: string
    };
    email!: string;
    birthday?: string;
    loginInfo!: {
      username: string,
      password: string,
      confirmPassword: string,
    };
  }