import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { Observable,pipe} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay} from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import {Customer} from '../models/customer.model';

@Injectable()
export class CustomerService {
  constructor(private http: HttpClient) {}

  saveUserInfo(formValue: Customer): Observable<boolean> {
      console.log(formValue);
    return this.http.post(`${environment.apiUrl}/user/store`, formValue).pipe(
      mapTo(true),
      delay(1000),
      // catchError(() => of(false).pipe(
      //   delay(1000)
      // ))
    );
  }

}