import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable,pipe} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay} from 'rxjs/operators';
import {AuthService} from '../../../core/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  loginForm!:FormGroup;
  email!:FormControl;
  password!:FormControl;
  loginError: boolean = false;
  loading = false;
  constructor(private fb:FormBuilder,private serviceAuth:AuthService,private router: Router){}

  ngOnInit(){
    this.controlsForm();
    this.MainForm();
  }
  controlsForm(){
  this.email= this.fb.control('',[Validators.required]);
  this.password= this.fb.control('',[Validators.required]);
  }
  MainForm(){
    this.loginForm= this.fb.group({
      email:this.email,
      password:this.password
    }) 
  }

  Onsubmit(){
    if(this.loginForm.value && this.loginForm.value.email!="" && this.loginForm.value.password!=""){
    this.loading = true;
     this.serviceAuth.login(this.loginForm.value).subscribe((isLoggedIn:any) => {
      this.loading = false;
      console.log('isLoggedIn',isLoggedIn);
      if (isLoggedIn) {
        this.loginError = false; 
        this.router.navigate(['/']);
        console.log('isLoggedIn',isLoggedIn);
        // Redirige vers la page de l'utilisateur connecté
      } else {
        this.loginError = true; 
        console.log(isLoggedIn);
        // Affiche un message d'erreur de login invalide
      }
      
    }); 
  }
    
  }
}
