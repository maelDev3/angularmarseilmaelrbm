import { Component,OnInit } from '@angular/core';
import {FormBuilder,FormControl ,Validators,FormGroup,AbstractControl} from '@angular/forms';
import {Observable,pipe} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay,startWith} from 'rxjs/operators';
import {CustomerService} from '../../services/customer.service';
import {confirmEqualValidator} from '../../validators/confirm-equal.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  mainForm!: FormGroup;
  formUser!: FormGroup;
  gender!: FormControl;
  email!:FormControl;
  birthday!: FormControl;

  passwordCtrl!: FormControl;
  confirmPasswordCtrl!: FormControl;
  loginInfoForm!: FormGroup;

  showPasswordError$!:Observable<boolean>;
  loading = false;
  



  constructor(private router:Router, private formBuilder: FormBuilder,private customerService: CustomerService) {
  }

  ngOnInit() {
    this.initFormControls();
    this.initMainForm();
    this.FormObservables();
  }

  private initFormControls(): void {
    this.formUser = this.formBuilder.group({
       firstName: ['', Validators.required],
       lastName: ['', Validators.required],
   });

   this.gender = this.formBuilder.control('Male');
   this.email  = this.formBuilder.control('',[Validators.required,Validators.email]);
   this.birthday = this.formBuilder.control('',Validators.required);
   this.passwordCtrl = this.formBuilder.control('', Validators.required);
   this.confirmPasswordCtrl = this.formBuilder.control('', Validators.required);
   this.loginInfoForm = this.formBuilder.group({
       username: ['', Validators.required],
       password: this.passwordCtrl,
       confirmPassword: this.confirmPasswordCtrl
   }, {
   validators: [confirmEqualValidator('password', 'confirmPassword')]
  });
  }
  
 private initMainForm(): void {
  this.mainForm = this.formBuilder.group({
        perso: this.formUser,
        sexe: this.gender,
        email:this.email,
        birthday:this.birthday,
        loginInfo: this.loginInfoForm

  });
}
private FormObservables() {
  this.showPasswordError$ = this.loginInfoForm.statusChanges.pipe(
    map(status => status === 'INVALID' &&
                  this.passwordCtrl.value &&
                  this.confirmPasswordCtrl.value  &&
                  this.loginInfoForm.hasError('confirmEqual')
    )
);

}

  onSubmitForm() {
    this.loading = true;
    this.customerService.saveUserInfo(this.mainForm.value).pipe(
      tap(saved => {
        this.loading = false;
          if (saved) {
          this.resetForm();
          this.router.navigate(['/']);
          } else {
            console.error('Echec de l\'enregistrement');
          }
      })
  ).subscribe();
}
private resetForm() {
  this.mainForm.reset();
  this.email.patchValue('email');
  this.birthday.patchValue('');
}

}
