export class Pays {
    id!: number;
    name!: string;
    code!: string;
    alpha3!: string;
    createdDate!: string;
  }