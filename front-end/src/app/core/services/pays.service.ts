import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, delay, Observable, tap,map,switchMap,take } from 'rxjs';
import { Pays } from '../models/pays.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class PaysService {
  constructor(private http: HttpClient) {}
  getPaysFromServer(): Observable<Pays[]>{
    return this.http.get<Pays[]>(`${environment.apiUrl}/pays`);
  }
}