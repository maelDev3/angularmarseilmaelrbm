import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable,pipe,of} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay,catchError} from 'rxjs/operators';
import {  HttpClient,HttpErrorResponse  } from '@angular/common/http';
// import { Customer} from '../../auth/models/customer.model'

export interface Customer {
  email: string;
  password: string;
}


@Injectable(
  {
  providedIn: 'root'
 }
)
export class AuthService {
  constructor(private http: HttpClient) {}
  private token!: string;

  login(form:Customer) : Observable<boolean>{
    return this.http.post(`${environment.apiUrl}/login`,form).pipe(
      tap((response: any) => {
        console.log(response);
        // Stockez le token renvoyé par Laravel dans localStorage ou sessionStorage
        if(response.token){
          localStorage.setItem('token', response.token);
          const userJSON = JSON.stringify(response.user);
          localStorage.setItem('user', userJSON);
        }
      }),
      mapTo(true),
      catchError((error: HttpErrorResponse) =>{
        console.log('Erreur de connexion :', error.error.message); // Affiche le message d'erreur de Laravel dans la console
        return of(false); // Renvoie un Observable avec la valeur false pour indiquer l'échec de connexion
      }));
    
  }

  logout() {
    // Supprimez le token du localStorage ou sessionStorage lors de la déconnexion
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    return of(true).pipe(delay(1000));
  }

  isLoggedIn() {
    // Vérifiez si le token existe dans le localStorage ou sessionStorage pour savoir si l'utilisateur est connecté ou non
    return !!localStorage.getItem('token');
  }
}