import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LanguageService {
  constructor(private translateService: TranslateService) {}
  switchLanguage(language: string): void {
    this.translateService.use(language);  
  }
}