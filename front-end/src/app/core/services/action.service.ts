import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, delay, Observable, tap,map,switchMap,take } from 'rxjs';
import { Action } from '../models/action.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class ActionService {
  constructor(private http: HttpClient) {}
  getAction(): Observable<Action[]>{
    return this.http.get<Action[]>(`${environment.apiUrl}/action`);
  }
}