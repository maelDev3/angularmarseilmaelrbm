import { Component,ViewChild, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import {environment } from '../../../../environments/environment';
import {LanguageService} from '../../services/language.service';
import { TranslateService } from '@ngx-translate/core';
 
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user!:any;
  pathUser=environment.pdp_user;

  ngOnInit(){
    const userJSON = localStorage.getItem('user');
    if (userJSON) {
      this.user = JSON.parse(userJSON);
      console.log(this.user);
    }
  }
constructor(private translateService: TranslateService,private authService: AuthService,private languageService:LanguageService) { }
  isLoggedIn(): boolean {
    // Vérifiez si le token existe dans le localStorage en utilisant le service AuthService
    return this.authService.isLoggedIn();
  }
  logout(){
    this.authService.logout()
  }
  switchLanguage(language: string): void {
    this.translateService.use(language);  
  }
  

}
