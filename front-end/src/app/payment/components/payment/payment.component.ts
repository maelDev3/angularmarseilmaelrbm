import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {map, startWith,tap} from 'rxjs/operators';
import {NgFor, AsyncPipe} from '@angular/common';
import {PackageService} from '../../../package/services/package.service';
import {PaymentService} from '../../services/payment.service';


import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  FormGroup,
  FormBuilder,
} from '@angular/forms';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit{
  paymentForm!:FormGroup;
  status!:FormControl;
  amount!:FormControl;
  payment_method!:FormControl;
  currency!:FormControl;
  type!:FormControl;
  package_id!:FormControl;
  packages:any[]=[];
  selected = new FormControl('onePhone', [Validators.required, Validators.pattern('onePhone')]);
 

  myControl = new FormControl();
  options: string[] = ['Retour', ' Point relais', 'Retrait', 'Facturation'];
  filteredOptions: Observable<string[]>;
  
  constructor(private fb:FormBuilder,private packageService:PackageService,private paymentService: PaymentService) {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value))
    );
  }

  ngOnInit() {
   this.initFormcontrol();
   this.initMainForm();
   this.fetchApiPackage();
  }
  initFormcontrol(){
    this.status  = this.fb.control('',[Validators.required]);
    this.amount = this.fb.control('',[Validators.required]);
    this.payment_method = this.fb.control('',[Validators.required]);
    this.currency = this.fb.control('',[Validators.required]);
    this.type = this.fb.control('',[Validators.required]);
    this.package_id = this.fb.control('',[Validators.required]);
  }
  initMainForm(){
    this.paymentForm=this.fb.group({
      status:this.status,
      amount:this.amount,
      payment_method:this.payment_method,
      currency:this.currency,
      type:this.type,
      package_id:this.package_id
    })
  }

 filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  Onsubmit(){
console.log(this.paymentForm.value);

this.paymentService.savePayement(this.paymentForm.value).pipe(
  tap(saved => {
    // this.loading = false;
      if (saved) {
        this.resetForm();
      } else {
        console.error('Echec de l\'enregistrement');
      }
  })
).subscribe();
  }
  fetchApiPackage()
  {
    this.packageService.getPackageFromServer().subscribe(
      (packages: any[]) => {
        this.packages = packages;
        console.log(this.packages);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  private resetForm() {
    this.paymentForm.reset();
    this.status.patchValue('');
    this.amount.patchValue('');
    this.payment_method.patchValue('');
    this.currency.patchValue('');
    this.type.patchValue('');
    this.package_id.patchValue('');
  }
}
