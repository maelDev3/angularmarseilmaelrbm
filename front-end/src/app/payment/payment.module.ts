import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentRoutingModule } from './payment-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PaymentComponent } from './components/payment/payment.component';
import { PackageService } from '../package/services/package.service';
import { PaymentService } from './services/payment.service';


@NgModule({
  declarations: [
    PaymentComponent
  ],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedModule
  ],
  providers:[
   PackageService,
   PaymentService
  ],
})
export class PaymentModule { }
