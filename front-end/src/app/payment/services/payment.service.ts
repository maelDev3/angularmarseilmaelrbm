import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { Observable,pipe} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay} from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Payment } from '../models/Payment.model';

@Injectable()
export class PaymentService {
  constructor(private http: HttpClient) {}
  savePayement(formValue: Payment): Observable<boolean> {
    return this.http.post(`${environment.apiUrl}/payment/store`, formValue).pipe(
      mapTo(true),
      delay(1000),
      // catchError(() => of(false).pipe(
      //   delay(1000)
      // ))
    );
  }
  
  getPayemtn(): Observable<Payment[]>{
    return this.http.get<Payment[]>(`${environment.apiUrl}/payment`);
  }
}