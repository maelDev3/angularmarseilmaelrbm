export class Payment {
   id!:number;
   status!:string;
   amount!:number;
   currency!:number;
   payment_method!:string;
   type!:string;
   package_id!:number;
  }