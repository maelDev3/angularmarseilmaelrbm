import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsComponent } from './components/comments/comments.component';
import { MaterialModule } from './material.module';
import {ReactiveFormsModule} from '@angular/forms';
import { ShortenPipe } from './pipes/shorten.pipe';
import { HighlightDirective } from './directives/highlight.directive';
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  declarations: [
    CommentsComponent,
    ShortenPipe,
    HighlightDirective,
    SpinnerComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports:[
    MaterialModule,
    CommentsComponent,
    ReactiveFormsModule,
    ShortenPipe,
    HighlightDirective,
    SpinnerComponent
  ]
})
export class SharedModule { }
