import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {map, startWith,tap} from 'rxjs/operators';
import {NgFor, AsyncPipe} from '@angular/common';
import { TransportService } from '../../../transport/services/transport.service';
import { ActionService } from '../../../core/services/action.service';
import {Transport} from '../../../transport/models/transport.model';
import {PackageService} from '../../services/package.service';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  FormGroup,
  FormBuilder,
} from '@angular/forms';



@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss'],

})
export class PackageComponent  implements OnInit{
  packageForm!:FormGroup;
  weight!:FormControl;
  volumetric_weight!:FormControl;
  type!:FormControl;
  transport_id!:FormControl;
  actionsForm!: FormGroup;
  myControl = new FormControl();
  options: string[] = ['One phone', 'Two phones', 'Normal package', 'Credit Card','Simple document'];
  filteredOptions: Observable<string[]>;
  transports: any[] = [];
  actions: any[] = [];
  loading = false;
  constructor(private transportService :TransportService,
    private actionService:ActionService,
    private fb: FormBuilder,
    private packageService:PackageService
    ) {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter(value))
    );
  }

  ngOnInit() {
    this.fetchTransport();
    this.fetchAction();
    this.initControl();
    this.initMainForm();
  }

 initObservable(){

 } 
 initMainForm(){
  this.packageForm = this.fb.group({
    weight: this.weight,
    volumetric_weight:this.volumetric_weight,
    type:this.type,
    transport_id:this.transport_id,
    // action_id:this.actionsForm
 });
 }
 initControl(){
  this.packageForm = this.fb.group({
    weight:['', Validators.required],
    volumetric_weight:['', Validators.required],
    action_id:this.actionsForm
  });
  this.type  = this.fb.control('',[Validators.required]);
  this.transport_id= this.fb.control('',[Validators.required]);

  // this.actionsForm = this.fb.group({
  //   action_id: ['', Validators.required],
  // });
       
 }

 filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  fetchTransport(): void {
    this.transportService.getTransportFromServer().subscribe(
      (tranports: any[]) => {
        this.transports = tranports;
        console.log(this.transports);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  fetchAction(): void {
    this.actionService.getAction().subscribe(
      (actions: any[]) => {
        this.actions = actions;
        console.log(this.actions);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  Onsubmit(){
    this.loading = true;
    console.log(this.packageForm.value);
    this.packageService.savePackage(this.packageForm.value).pipe(
      tap(saved => {
        this.loading = false;
          if (saved) {
          } else {
            console.error('Echec de l\'enregistrement');
          }
      })
  ).subscribe();
  }
}
