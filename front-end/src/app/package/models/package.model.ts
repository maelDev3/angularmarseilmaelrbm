export class Package {
  id!: number;
  warehouse_id!: number;
  user_id!: number;
  weight!: number;
  transport_id!: number;
  volumetric_weight!:number;
  type!:string;
}