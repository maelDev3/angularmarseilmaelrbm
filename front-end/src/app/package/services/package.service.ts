import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';

import { tap ,map,switchMap,mapTo,delay} from 'rxjs/operators';
import {Observable,startWith,combineLatest,take } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Package } from '../models/package.model';

@Injectable()
export class PackageService {
constructor(private http: HttpClient) {}

 getPackageFromServer(): Observable<Package[]>{
    return this.http.get<Package[]>(`${environment.apiUrl}/package`);
  }
  
  savePackage(formValue: Package): Observable<boolean> {
    return this.http.post(`${environment.apiUrl}/package/store`, formValue).pipe(
      mapTo(true),
      delay(1000),
      // catchError(() => of(false).pipe(
      //   delay(1000)
      // ))
    );
  }
} 