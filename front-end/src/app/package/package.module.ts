import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageRoutingModule } from './package-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PackageComponent } from './components/package/package.component';
import { PackageService } from './services/package.service';
import { TransportService } from '../transport/services/transport.service';
import { ActionService } from '../core/services/action.service';
@NgModule({
  declarations: [
    PackageComponent
  ],
  imports: [
    CommonModule,
    PackageRoutingModule,
    SharedModule,
  ],
  providers:[
    PackageService,
    TransportService,
    ActionService
  ],
})
export class PackageModule { }
