import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransportRoutingModule } from './transport-routing.module';
import { TransportComponent } from './components/transport/transport.component';
import { SharedModule } from '../shared/shared.module';
import { TransportService } from './services/transport.service';

@NgModule({
  declarations: [
    TransportComponent
  ],
  imports: [
    CommonModule,
    TransportRoutingModule,
    SharedModule
  ],
  providers:[
    TransportService,
  ],
})
export class TransportModule { }
