import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { Observable,pipe} from 'rxjs';
import { tap ,map,switchMap,mapTo,delay} from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Transport } from '../models/transport.model';

@Injectable()
export class TransportService {
  constructor(private http: HttpClient) {}
  saveTransport(formValue: Transport): Observable<boolean> {
    return this.http.post(`${environment.apiUrl}/transport/store`, formValue).pipe(
      mapTo(true),
      delay(1000),
      // catchError(() => of(false).pipe(
      //   delay(1000)
      // ))
    );
  }
  
  getTransportFromServer(): Observable<Transport[]>{
    return this.http.get<Transport[]>(`${environment.apiUrl}/transport`);
  }
}