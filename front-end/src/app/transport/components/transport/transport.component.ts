import { Component,OnInit } from '@angular/core';
import {PaysService} from '../../../core/services/pays.service';
import {Pays} from '../../../core/models/pays.model';
import {Observable,startWith,map,combineLatest,take ,tap} from 'rxjs';
import {FormBuilder,FormControl ,Validators,FormGroup,AbstractControl} from '@angular/forms';
import {TransportService} from '../../services/transport.service';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['./transport.component.scss']
})
export class TransportComponent implements OnInit{
  pays: any[] = [];
  formTransport!:FormGroup;
  name!:FormControl;
  address!:FormControl;
  ville!:FormControl;
  codePostal!:FormControl;
  pays_id!:FormControl;
  
  loading = false;
      
  constructor(private paysService: PaysService,private formBuilder: FormBuilder,private transportService: TransportService) { }

  ngOnInit(){
    this.fetchPays();
    this.initFormControls();
    this.mainForm();
  }
  fetchPays(): void {
    this.paysService.getPaysFromServer().subscribe(
      (pays: any[]) => {
        this.pays = pays;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  private mainForm(){
    this.formTransport = this.formBuilder.group({
      name: this.name,
      address: this.address,
      ville:this.ville,
      pays_id:this.pays_id,
      codePostal: this.codePostal

});
  }
  private initFormControls(): void {
    this.name  = this.formBuilder.control('',[Validators.required]);
    this.address  = this.formBuilder.control('',[Validators.required]);
    this.ville  = this.formBuilder.control('',[Validators.required]);
    this.codePostal  = this.formBuilder.control('',[Validators.required]);
    this.pays_id  = this.formBuilder.control('',[Validators.required]);
  }
  onSubmit(){
    this.loading = true;
    console.log(this.formTransport.value);
    this.transportService.saveTransport(this.formTransport.value).pipe(
      tap(saved => {
        this.loading = false;
          if (saved) {
          } else {
            console.error('Echec de l\'enregistrement');
          }
      })
  ).subscribe();
  }
}
