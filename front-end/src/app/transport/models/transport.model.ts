import { Pays } from '../../core/models/pays.model';
export class Transport {
  id!: number;
  Name!: string;
  address!: string;
  codePostal!: string;
  pays_id!: Pays;
}