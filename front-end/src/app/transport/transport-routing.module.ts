import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TransportComponent} from './components/transport/transport.component';

const routes: Routes = [
  { path: 'create', component: TransportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransportRoutingModule { }
