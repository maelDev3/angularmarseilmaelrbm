
    <div class="site-menubar">
      <div class="site-menubar-header">
        <div class="cover overlay">
          <img class="cover-image" src="{{asset('assets//examples/images/dashboard-header.jpg')}}"
            alt="...">
          <div class="overlay-panel vertical-align overlay-background">
            <div class="vertical-align-middle">
              <a class="avatar avatar-lg" href="#">
                <img src="{{asset('global/portraits/FF.jpg')}}" alt="">
              </a>
              <div class="site-menubar-info">
                <h5 class="site-menubar-user">Mael</h5>
                <p class="site-menubar-email">maeldev3@gmail.com</p>
              </div>
            </div>
          </div>
        </div>
      </div>  <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-item active">
                <a class="animsition-link" href="{{ asset(route('dashboard')) }}">
                        <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                        <span class="site-menu-title">Dashboard</span>
                    </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="#">
                        <i class="site-menu-icon md-view-compact" aria-hidden="true"></i>
                         {{-- trans('menu.user') --}} 
                        <span class="site-menu-title">Utilisateur</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{ asset(route('user.index')) }}">
                      <span class="site-menu-title">Gestion Utilisateur</span>
                    </a>
                  </li>
                 
                  
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{asset(route('forget-password'))}}">
                      <span class="site-menu-title">Mot de pass oublier</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{asset(route('role.create'))}}">
                      <span class="site-menu-title">Rôle</span>
                    </a>
                  </li>
                
                </ul>
              </li>
              <li class="site-menu-item has-sub">
                <a href="#">
                        <i class="site-menu-icon md-google-pages" aria-hidden="true"></i>
                        <span class="site-menu-title">Chat</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{asset(route('chat.index'))}}">
                      <span class="site-menu-title">Chat</span>
                    </a>
                  </li>
               
                 
                 
                 
                </ul>
              </li>
              <li class="site-menu-item has-sub">
                <a href="#">
                        <i class="site-menu-icon md-palette" aria-hidden="true"></i>
                        <span class="site-menu-title">Produits</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item has-sub">
                    <a href="#">
                      <span class="site-menu-title">Categorie</span>
                      <span class="site-menu-arrow"></span>
                    </a>
                    <ul class="site-menu-sub">
                      <li class="site-menu-item">
                        <a class="animsition-link" href="uikit/panel-structure.html">
                          <span class="site-menu-title">Liste Produits</span>
                        </a>
                      </li>
                    
                    </ul>
                  </li>
                 
                </ul>
              </li>
              <!-- <li class="site-menu-item has-sub">
                <a href="#">
                        <i class="site-menu-icon md-format-color-fill" aria-hidden="true"></i>
                        <span class="site-menu-title">Transporteur</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item hidden-sm-down site-tour-trigger">
                    <a href="{{route('transport.index')}}">
                      <span class="site-menu-title">Transporteur</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{route('action.index')}}">
                      <span class="site-menu-title">Action</span>
                    </a>
                  </li>
                </ul>
              </li> -->
              <li class="site-menu-item has-sub">
                <a href="#">
                        <i class="site-menu-icon md-puzzle-piece" aria-hidden="true"></i>
                        <span class="site-menu-title">Project</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{asset(route('portofolio'))}}">
                      <span class="site-menu-title">Portofolio</span>
                    </a>
                  </li>
                </ul>
              </li> 
            
              
             
                </ul>
              </li>
            </ul>     
            </div>
        </div>
      </div>
    </div>