@extends('dashboard.master')
@push('page_title')
    Ajout action
@endpush

@push('css')

@endpush

@section('content')

<div class="d-flex justify-content-center mt-5">
    <div class="panel col-md-4">
      <div class="panel-heading">
        <h3 class="panel-title">Action</h3>
      </div>
      <div class="panel-body container-fluid">
        <form autocomplete="off" method="POST" action="{{route('action.store')}}">
           @csrf
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="text" class="form-control empty" name="name">
            <label class="floating-label">Name</label>
          </div>
          <button class="float-right btn btn-info waves-effect waves-light waves-round" type="submit">Ajouter</button>
          </div>
        </form>
      </div>
    </div>
</div>

@endsection

@push('js')

@endpush