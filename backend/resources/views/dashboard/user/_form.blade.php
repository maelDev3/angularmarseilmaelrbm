<div class="panel">
          <div class="panel-body container-fluid">
            <div class="d-flex justify-content-center">
              <div class="col-md-6">
                <div class="example-wrap">
                  <h4 class="example-title">Sigin up</h4>
                  <div class="example">
                    <form autocomplete="off">
                      <div class="row">
                        <div class="form-group form-material col-md-6">
                          <label class="form-control-label" for="inputBasicFirstName">First Name</label>
                          <input type="text" class="form-control" id="inputBasicFirstName" name="firstname" placeholder="First Name" autocomplete="off">
                        </div>
                        <div class="form-group form-material col-md-6">
                          <label class="form-control-label" for="inputBasicLastName">Last Name</label>
                          <input type="text" class="form-control" id="inputBasicLastName" name="lastname" placeholder="Last Name" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group form-material">
                        <label class="form-control-label">Gender</label>
                        <div>
                          <div class="radio-custom radio-default radio-inline">
                            <input type="radio" id="inputBasicMale" name="gender" value="Male">
                            <label for="inputBasicMale">Male</label>
                          </div>
                          <div class="radio-custom radio-default radio-inline">
                            <input type="radio" id="inputBasicFemale" name="gender" value="Female">
                            <label for="inputBasicFemale">Female</label>
                          </div>
                          <div class="radio-custom radio-default radio-inline">
                            <input type="radio" id="inputBasicFemale" name="gender" value="Non-specifique">
                            <label for="inputBasicFemale">Non-Specifique</label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group form-material">
                        <label class="form-control-label" for="inputBasicEmail">Email Address</label>
                        <input type="email" class="form-control" id="inputBasicEmail" name="email" placeholder="Email Address" autocomplete="off">
                      </div>
                      <div class="form-group form-material">
                    <label class="form-control-label">Date de naissance</label>
                            <input type="text" class="form-control" id="inputDate2" name="birthday" data-plugin="formatter" data-pattern="[[99]]/[[99]]/[[9999]]">
                            
                        </div>
                      <div class="form-group form-material">
                        <label class="form-control-label" for="inputBasicPassword">Password</label>
                        <input type="password" class="form-control" id="inputBasicPassword" name="password" placeholder="Password" autocomplete="off">
                      </div>

                      <div class="form-group form-material">
                        <label class="form-control-label" for="inputBasicPassword">Confirm</label>
                        <input type="password" class="form-control" id="inputBasicPassword" name="confirme" placeholder="Password" autocomplete="off">
                      </div>

                      <div class="">
                      <div class="dropify-message">
                      <span class="file-icon">
                      </span> <p>Photos de profile</p>
                      <p class="dropify-error"></p></div>
                      <div class="dropify-loader" style="display: none;"></div>
                      <div class="dropify-errors-container"><ul></ul></div>
                      <input type="file" id="input-file-now" name="image" data-plugin="dropify" data-default-file="">
                      
                      <div class="dropify-preview" style="display: none;">
                      <span class="dropify-render"></span><div class="dropify-infos">
                      <div class="dropify-infos-inner"><p class="dropify-filename">
                      <span class="file-icon"></span>
                       <span class="dropify-filename-inner">hands-1150073_960_720.jpg</span></p>
                       <p class="dropify-infos-message">Drag and drop or click to replace</p>
                       </div></div></div></div>

                      <div class="form-group form-material">
                        <div class="checkbox-custom checkbox-default">
                          <input type="checkbox" id="inputBasicRemember" name="inputCheckbox" checked="" autocomplete="off">
                          <label for="inputBasicRemember">Remember Me</label>
                        </div>
                      </div>
                      <div class="form-group form-material">
                        <button type="submit" class="btn btn-primary waves-effect waves-light waves-round">Sign Up</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
             

             
            </div>
          </div>
        </div>