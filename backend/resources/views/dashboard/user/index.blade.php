@extends('dashboard.master')
@push('page_title')
    Liste Utilisateur
@endpush
@push('css')
<link rel="stylesheet" href="{{asset('global/vendor/footable/footable.core.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/tables/footable.css')}}">
<style>
td.td-image {
    justify-content: center;
    display: flex!important;
}

.pdpImage{
    margin-right: 4px;
    border: 1px solid #fff;
    border-radius: 50%;
    width: 50px;
    height: 50px;
}
.bgStrname{
    background-color: #00bcd4;
    border: 1px solid #fff;
    border-radius: 50%;
    width: 50px;
    height: 50px;
}

span.strname {
    justify-content: center;
    color: white;
    display: flex;
    margin-top: 12px;
}


</style>
@endpush

@section('content')
<div class="panel">
              
              <header class="panel-heading">
                <h3 class="panel-title">Liste Utilisateur</h3>
              </header>

              <div class="mr-4 float-right mb-2">
              <a type="button" class="btn btn-info btn-sm waves-effect waves-light waves-round mr-1" href="{{route('user.create')}}">Ajouter</a>
              </div>
              <div class="panel-body">
                <table class="table table-bordered table-hover toggle-circle footable footable-9 footable-filtering footable-filtering-right footable-paging footable-paging-center breakpoint-lg" id="exampleFootableFiltering" data-paging="true" data-filtering="true" data-sorting="true" style="">
                  <thead>
                  <tr class="footable-filtering">
                    <th colspan="7"></th>
                  </tr>
                    <tr class="footable-header">
                    <th data-name="id" data-type="number" data-breakpoints="xs" class="footable-sortable footable-first-visible" style="display: table-cell;">ID<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="firstName" class="footable-sortable" style="display: table-cell;">Image<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="firstName" class="footable-sortable" style="display: table-cell;">First Name<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="lastName" class="footable-sortable" style="display: table-cell;">Last Name<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="something" data-visible="false" data-filterable="false" class="footable-sortable" style="display: none;">Never seen but always around<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="jobTitle" data-breakpoints="xs sm" class="footable-sortable" style="display: table-cell;">Job Title<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="started" data-type="date" data-breakpoints="xs sm md" data-format-string="MMM YYYY" class="footable-sortable" style="display: table-cell;">Started On<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="dob" data-type="date" data-breakpoints="xs sm md" data-format-string="DD MMM YYYY" class="footable-sortable" style="display: table-cell;">Date of Birth<span class="fooicon fooicon-sort"></span></th>
                    <th data-name="status" class="footable-sortable footable-last-visible" style="display: table-cell;">Status<span class="fooicon fooicon-sort"></span></th></tr>
                  </thead>
                  <tbody>

                  @foreach($users as $user)
                  <tr>
                  <td class="footable-sortable" style="display: table-cell;">
                     {{$user->id}}
                  </td>

                  <td style="display:flex;" class="td-image">
                      @if(count($user->PhotosProfil))
                        <img alt="image"  class="pdpImage" src="{{ asset('Image/User/'.$user->PhotosProfil[0]['photos'])}}" width="40px" height="40px">
                      @else 
                      <div class="bgStrname"> <span class="strname">{{$user->str_name ? $user->str_name : 'MR'}}</span></div>
                      @endif    
                  </td>

                  <td style="display: table-cell;">{{ $user->name }}</td>
                  <td style="display: table-cell;">{{ $user->firstname }}</td>
                  <td style="display: none;">1381105566987</td>
                  <td style="display: table-cell;">Airline Transport Pilot</td>
                  <td style="display: table-cell;">May 2013</td>
                  <td style="display: table-cell;">17 Nov 1973</td>
                  <td class="footable-last-visible" style="display: table-cell;">
                         
                         @if($user->status == "Active")
                         <span class="badge badge-table badge-success">Active</span>
                         @elseif($user->status == "Suspended") 
                        <span class="badge badge-table badge-danger">Suspended</span>
                        @else
                        <span class="badge badge-table badge-dark">Deleted</span>
                        @endif
                      </td>
                </tr>
                  <tr>
                  </tr>
                  @endforeach  
                    </tbody>
                  <tfoot>


                      <td colspan="5">
                        <div class="text-right">
                          <ul class="pagination"></ul>
                        </div>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
@endsection
@push('js')
<!-- <script src="{{asset('global/vendor/moment/moment.min.js')}}"></script> -->
<script src="{{asset('global/vendor/footable/footable.min.js')}}"></script>
<script src="{{asset('assets/examples/js/tables/footable.js')}}"></script>
@endpush