@extends('dashboard.master')
@push('page_title')
    Ajout Utilisateur
@endpush

@push('css')
<link rel="stylesheet" href="{{asset('global/vendor/dropify/dropify.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/forms/masks.css')}}">
@endpush

@section('content')

<div class="page-content">
     <form method="post" action="{{route('user.store')}}" enctype="multipart/form-data">
        @csrf
        @include('dashboard.user._form');
     </form>
</div>
@endsection  

@push('js')
  <script src="{{asset('global/vendor/dropify/dropify.min.js')}}"></script>
  <script src="{{asset('global/vendor/formatter/jquery.formatter.js')}}"></script>
  <script src="{{asset('global/js/Plugin/formatter.js')}}"></script>
  <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
@endpush