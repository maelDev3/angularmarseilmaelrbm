@extends('dashboard.master')
@push('page_title')
    Forget Password
@endpush
@section('content')
    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content vertical-align-middle">
        <h2>Forgot Your Password ?</h2>
        <p>Input your registered email to reset your password</p>

        <form method="post" role="form" autocomplete="off">
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="email" class="form-control empty" id="inputEmail" name="email">
            <label class="floating-label" for="inputEmail">Your Email</label>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Reset Your Password</button>
          </div>
        </form>

        <footer class="page-copyright">
          <!-- <p>WEBSITE BY Creation Studio</p>
          <p>© 2018. All RIGHT RESERVED.</p> -->
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-twitter" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-facebook" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-google-plus" aria-hidden="true"></i>
        </a>
          </div>
        </footer>
      </div>
    </div>
    <!-- End Page -->
@endsection
