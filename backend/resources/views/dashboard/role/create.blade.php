@extends('dashboard.master')
@push('page_title')
    Ajout Rôle
@endpush


@section('content')
<div class="panel-body container-fluid ">
            <div class="row row-lg d-flex justify-content-center">
              <div class="col-md-6">
                <!-- Example Input Sizing -->
                <div class="example-wrap">
                  <h4 class="example-title">Ajout rôle</h4>
                  <div class="example">
                    <input type="text" class="form-control" placeholder="Name">
                  </div>
                  <div class="example">
                    <input type="text" class="form-control" placeholder="Description">
                  </div>

                  <div class="example">
                     <button class="btn btn-primary">Ajouter</button>
                  </div>
                </div>
                <!-- End Example Input Sizing -->
              </div>

            
            </div>
          </div>
@endsection          