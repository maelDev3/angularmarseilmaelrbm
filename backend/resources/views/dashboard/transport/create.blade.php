@extends('dashboard.master')
@push('page_title')
    Ajout Transport
@endpush

@push('css')

@endpush

@section('content')
<div class="d-flex justify-content-center mt-5">
    <div class="panel col-md-4">
      <div class="panel-heading">
        <h3 class="panel-title">Transport</h3>
      </div>
      <div class="panel-body container-fluid">
        <form autocomplete="off" method="POST" action="{{route('transport.store')}}">
           @csrf
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="text" class="form-control empty" name="name">
            <label class="floating-label">Name</label>
          </div>
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="text" class="form-control empty" name="address">
            <label class="floating-label">Address</label>
          </div>
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="text" class="form-control empty" name="ville">
            <label class="floating-label">Ville</label>
          </div>
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <input type="text" class="form-control empty" name="code_postal">
            <label class="floating-label">Code Postal</label>
          </div>
          
          <div class="form-group form-material floating" data-plugin="formMaterial">
            <select class="form-control" name="pays_id">
              <option>&nbsp;</option>
              @foreach($pays as $item)
              <option value="{{$item->id}}">{{$item->name}}</option>
              @endforeach
            </select>
            <label class="floating-label">Pays</label>
          </div>
          <button class="float-right btn btn-info waves-effect waves-light waves-round" type="submit">Ajouter</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection

@push('js')

@endpush