<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/', function () {
    return view('dashboard.master');
});

// Route::middleware(['web'])->group(function(){
//     Auth::routes();
// });
Auth::routes();
require_once 'web/login.php';
require_once 'web/dashboard.php';
require_once 'web/user.php';
require_once 'web/transport.php';
require_once 'web/action.php';
require_once 'web/chat.php';
require_once 'web/forget-password.php';
require_once 'web/role.php';






// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
