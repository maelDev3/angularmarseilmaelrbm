<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
require_once 'api/login.php';
require_once 'api/user.php';
require_once 'api/transport.php';
require_once 'api/pays.php';
require_once 'api/action.php';
require_once 'api/package.php';
require_once 'api/payment.php';
require_once 'api/menu.php';
