<?php
use App\Http\Controllers\Api\ActionController;
use Illuminate\Support\Facades\Route;
Route::prefix('action')
// ->middleware('auth:sanctum')
->name('action.')->controller(ActionController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
    });
