<?php
use App\Http\Controllers\Api\PackagesController;
use Illuminate\Support\Facades\Route;
Route::prefix('package')->name('package.')->controller(PackagesController::class)
    ->group(function () {
        Route::post('store', 'store')->name('store');
        Route::get('', 'index')->name('index');
    });
