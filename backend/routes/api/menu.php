<?php
use App\Http\Controllers\Api\MenuController;
use Illuminate\Support\Facades\Route;
Route::prefix('menu')->name('menu.')->controller(MenuController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::post('', 'store')->name('store');
        Route::get('{id}/edit', 'edit')->name('edit');
        Route::put('{id}/edit', 'update')->name('update');
        Route::put('{id}/delete', 'delete')->name('delete');
    });
