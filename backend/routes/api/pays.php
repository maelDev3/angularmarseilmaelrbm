<?php
use App\Http\Controllers\Api\PaysController;
use Illuminate\Support\Facades\Route;
Route::prefix('pays')->name('pays.')->controller(PaysController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
    });
