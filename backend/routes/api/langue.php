<?php
use App\Http\Controllers\Api\LoginController;
use Illuminate\Support\Facades\Route;

Route::get('/greeting/{locale}', function (string $locale) {
    if (! in_array($locale, ['en', 'es', 'fr'])) {
        abort(400);
    }
    App::setLocale($locale);
    // Get a greeting message based on the selected locale
    $greeting = trans('greetings.greet');
    return view('welcome', ['greeting' => $greeting]);
});
