<?php
use App\Http\Controllers\Api\PaymentController;
use Illuminate\Support\Facades\Route;
Route::prefix('payment')->name('payment.')->controller(PaymentController::class)
    ->group(function () {
        Route::post('store', 'store')->name('store');
        Route::get('', 'index')->name('index');
    });
