<?php
use App\Http\Controllers\Api\TransportController;
use Illuminate\Support\Facades\Route;
Route::prefix('transport')->name('transport.')->controller(TransportController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::post('store', 'store')->name('store');
    });
