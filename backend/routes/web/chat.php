<?php
use App\Http\Controllers\Admin\ChatController;
use Illuminate\Support\Facades\Route;
Route::prefix('chat')->name('chat.')->controller(ChatController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('messages', 'fecthMessage')->name('fecthMessage');
        Route::post('messages', 'sendMessage')->name('sendMessage');
    });

