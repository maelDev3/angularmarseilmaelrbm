<?php
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
Route::prefix('user')->name('user.')->controller(UserController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('create', 'create')->name('create');
        Route::post('store', 'store')->name('store');
        Route::put('update/id', 'update')->name('update');
        Route::get('edit/id', 'edit')->name('edit');
        
    });

