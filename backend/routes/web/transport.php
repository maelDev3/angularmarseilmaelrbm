<?php
use App\Http\Controllers\Admin\TransportController;
use Illuminate\Support\Facades\Route;
Route::prefix('transport')->name('transport.')->controller(TransportController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('create', 'create')->name('create');
        Route::post('store', 'store')->name('store');
        Route::get('edit/1', 'edit')->name('edit');
        Route::put('update/1', 'update')->name('update');
    });
