<?php
use App\Http\Controllers\Admin\RoleController;
use Illuminate\Support\Facades\Route;
Route::prefix('role')->name('role.')->controller(RoleController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('create', 'create')->name('create');
    });