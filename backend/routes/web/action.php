<?php
use App\Http\Controllers\Admin\ActionController;
use Illuminate\Support\Facades\Route;
Route::prefix('action')->name('action.')->controller(ActionController::class)
    ->group(function () {
        Route::get('', 'index')->name('index');
        Route::get('create', 'create')->name('create');
        Route::post('store', 'store')->name('store');
        Route::get('edit/1', 'edit')->name('edit');
        Route::put('update/1', 'update')->name('update');
    });
