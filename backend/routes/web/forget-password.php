<?php
use App\Http\Controllers\Admin\AuthController;
use Illuminate\Support\Facades\Route;
Route::prefix('forget-password')->controller(AuthController::class)
    ->group(function () {
        Route::get('', 'index')->name('forget-password');
    });
