<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageEvent;
use App\Models\Message;
class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        
        return view('dashboard.chat.index');
    }
    public function fecthMessage(){
        return Message::with('user')->get();
    }
    public function sendMessage(Request $request){
        
        $user = Auth::user();
        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'status' => true,
            'conversation_id' => 1,
        ]);
        broadcast(new MessageEvent($user, $message))->toOthers();
        return redirect()->route('chat.index');
    }
}
