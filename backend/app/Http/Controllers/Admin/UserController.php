<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Services\FileSysteme;

class UserController extends Controller
{
    public function __construct(private FileSysteme $fileSysteme)
    {

    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::with([
            'profile',
            'PhotosProfil'=>function($q){
               $q->where('is_actif',1);
           }])->select(['id','name','firstname','email','is_online','job','status'])->get();
       
        return view('dashboard.user.index',['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('dashboard.user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $status = array('Suspended', 'Deleted', 'Active');
            $user = User::create([
                'firstname' => $request->firstname,
                'username'  => $request->name,
                'name'      => $request->lastname,
                'email'     => $request->email,
                'birthday'  => $request->birthday,
                'job'  => $request->job ? $request->job : "Airline Transport Pilot",
                'password'  => Hash::make($request->password),
                'status'    => $status[array_rand($status)],
                'is_online' => "1",
            ]);
             
            if($request->image){
             $image = $this->fileSysteme->addImage('Image/User',$request->image,'pdp');
                $user->PhotosProfil()->create([
                    'photos'    => $image,
                    'is_actif' => true
                ]);
            }
            $token = $user->createToken('laravel')->plainTextToken;
            
            $colors = array('#282E33', '#25373A', '#164852', '#495E67', '#FF3838','#457234', '#514543', '#690493',"#049383");
            $user->profile()->create([
                'sexe'    => $request->gender,
                'bgColor' => $colors[array_rand($colors)],
            ]);

            
             
    
        } catch (\Throwable $th) {
            throw $th;
        }
        
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        dd('edit');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        dd('update');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        dd('destroy');
    }
}
