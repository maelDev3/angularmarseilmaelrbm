<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Transporteur,pays};

class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('dashboard.transport.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pays = Pays::select('id','name','code','alpha3')->get();
        return view('dashboard.transport.create',['pays'=>$pays ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data["name"]        = $request['name'];
        $data["address"]     = $request['address'];
        $data["ville"]       = $request['ville'];
        $data["code_postal"] = $request['code_postal'];
        $data["pays_id"]     = $request['pays_id'];

        Transporteur::create($data);

        return redirect()->route('transport.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
