<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;
use Illuminate\Support\Srt;
class MenuController extends Controller
{
    public function index(){
        $menus = Menu::get();
        return response()->json($menus);
    }
    public function store(Request $request){
         $data['title']     = $request['title'];
         $data['icon']      = $request['icon'];
         $data['url']       = $request['url'];
         $data['position']  = $request['position'];
         // $data['slug']      = Str::slug($request['title']);
         $data['parent_id'] = null;
         
         $menu = Menu::create($data);

        return response()->json([
            "message" => 'created Menu',
            "data"    => $menu 
        ]);
    }
    public function edit($id){
        $menu = Menu::find($id);
        return response()->json($menu);
    }
    public function show($id){
        $menu = Menu::find($id);
        return response()->json($menu);
    }
    public function update(Request $request,$id){
        $menu = Menu::find($id);
        $menu->title       = $request['title'];
        $menu->icon        = $request['icon'];
        $menu->url         = $request['url'];
        $menu->title       = $request['title'];
        // $menu->position_id = $request['position_id'];
        // $menu->slug        = $request['slug'];
        $menu->save();

        return response()->json([
            "message" => 'Update Menu',
            "data"    => $menu 
        ]);
    }

    public function delete($id){
        $menu = Menu::find($id);
        $menu->delete();
        return response()->json([
            "message" => 'Menu deleted',
        ]);
    }
    
}
