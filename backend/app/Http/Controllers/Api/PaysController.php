<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pays;
class PaysController extends Controller
{
    public function index(){
       $pays = Pays::select('id','name','code','alpha3')->get();
       return response()->json($pays);
    }
}
