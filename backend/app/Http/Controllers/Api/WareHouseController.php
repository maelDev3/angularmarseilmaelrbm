<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Warehouse;

class WareHouseController extends Controller
{
    public function store(Request $request){
        $warehouse = Warehouse::create([
            'no' => $request->no
         ]);
        return response()->json([
            "message" => 'created warehouse',
            "data"    => $warehouse
        ]);
    }
}
