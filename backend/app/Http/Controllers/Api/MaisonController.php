<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Maison;

class MaisonController extends Controller
{
    public function store($request){
        $data["dash_login"]  = $request['dash_login'];
        $data["address"]     = $request['address'];
        $data["ville"]       = $request['ville'];
        $data["pays"]        = $request['pays'];
        $data["code_postal"] = $request['code_postal'];
        $data["pays_id"]     = $request['pays_id'];

        $maison = Maison::create($data);
        
        return response()->json([
            "message" => 'created Maison',
            "data"    => $maison
        ]);
    }

}
