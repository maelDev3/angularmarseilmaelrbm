<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transporteur;
use Illuminate\Support\Facades\DB;
class TransportController extends Controller
{
    public function index(){
      $query = DB::table('transporteurs')->get();
      return response()->json($query);
    }
    public function store(Request $request){
        $data["name"]        = $request['name'];
        $data["address"]     = $request['address'];
        $data["ville"]       = $request['ville'];
        $data["code_postal"] = $request['codePostal'];
        $data["pays_id"]     = $request['pays_id'];
        Transporteur::create($data);
        return response()->json([
            "message" => 'created Transporteur',
            "data"    => $data
        ]);
    }

}
