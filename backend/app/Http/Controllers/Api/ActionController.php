<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Action;

class ActionController extends Controller
{
    public function index(){
        $action = Action::select('name')->get();
        return response()->json($action);
    }

    public function store(Request $request){
        $action = Action::create([
            'name' => $request->name
         ]);
        return response()->json([
            "message" => 'created Action',
            "data"    => $action
        ]);
    }

}
