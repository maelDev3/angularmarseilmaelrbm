<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Logincontroller extends Controller
{
    
    public function authentificate(Request $request)
    {
        
        try {
            $user = User::whereEmail($request->email)->first();
            if (!$user) {
                return [
                    'error' => 'User not exist',
                    'status_code' => 404
                ];
            }
            if ($user->password) {
                if (!Hash::check($request->password, $user->password)) {
                    return [
                        'error' => 'Credentials wrong',
                        'message' => 'Credentials wrong',
                        'status_code' => 401
                    ];
                }
            }
             // Charger les photos de profil associées à l'utilisateur
              $userWithPhotos = $user->load('PhotosProfil');

            return response()->json([
                'token' => $user->createToken(time())->plainTextToken,
                'user' => $userWithPhotos,
            ]);
        } catch (Exception $e) {
            return [
                'error' => 'something_went_wrong',
                'message' => $e->getMessage(),
                'status_code' => 500
            ];
        }
    }
}
