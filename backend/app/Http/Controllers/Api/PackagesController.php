<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{PackageAction,Packages};
use Illuminate\Support\Facades\DB;
class PackagesController extends Controller
{
    public function index(){
        $query = DB::table('packages')->get();
        return response()->json($query);
      }

    public function store(Request $request){
        
        $data['weight']            = $request->weight;
        $data['type']              = $request->type;
        $data['user_id']           = $request->user_id;
        $data['warehouse_id']      = $request->warehouse_id;
        $data['transport_id']      = $request->transport_id;
        $data['volumetric_weight'] = $request->volumetric_weight;
        
        $package = Packages::create($data);

        if($request['action_id'])
        $package->actions()->create([
            'action_id' => $request['action_id']
        ]);

        return response()->json([
            "message" => 'created package',
            "data"    => $package
        ]);
    }
}
