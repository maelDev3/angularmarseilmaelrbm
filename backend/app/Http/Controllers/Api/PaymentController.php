<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Payment,Packages};
use Illuminate\Support\Facades\DB;
class PaymentController extends Controller
{

    public function index(){
        $query = DB::table('payments')->get();
        return response()->json();
    }
    public function store(Request $request){
       $data['status']         = $request->status;
       $data['amount']         = $request->amount;
       $data['currency']       = $request->currency;
       $data['payment_method'] = $request->payment_method;
       $data['package_id']     = $request->package_id;
       $data['type']           = $request->type;
       $payment = Payment::create($data);
       return response()->json([
           "message" => 'created payment',
           "data"    => $payment
        ]);
    }
}
