<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $status = array('Suspended', 'Disabled', 'Active');
            $user = User::create([
                'firstname' => $request->perso['firstName'],
                'username'  => $request->loginInfo['username'],
                'name'      => $request->perso['lastName'],
                'email'     => $request->email,
                'birthday'  => $request->birthday,
                'job'  => $request->job ? $request->job : "Airline Transport Pilot",
                'password'  => Hash::make($request->loginInfo['password']),
                'status'    => $status[array_rand($status)],
                'is_online' => "1",
            ]);
             
            if($request->image){
             $image = $this->fileSysteme->addImage('Image/User',$request->image,'pdp');
                $user->PhotosProfil()->create([
                    'photos'    => $image,
                    'is_actif' => true
                ]);
            }
            $colors = array('#282E33', '#25373A', '#164852', '#495E67', '#FF3838','#457234', '#514543', '#690493',"#049383");
            $user->profile()->create([
                'sexe'    => $request->gender,
                'bgColor' => $colors[array_rand($colors)],
                "color"   => "white"
            ]);
    
        } catch (\Throwable $th) {
            throw $th;
        }
        
        return response()->json($user);
    }

   
    
}
