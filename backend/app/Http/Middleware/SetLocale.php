<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
         // Set the locale based on the user's preference, stored in the session or any other source
         $locale = session('locale', 'en'); // Default to English if not set

         // Set the application locale
         app()->setLocale($locale);
         
        return $next($request);
    }
}
