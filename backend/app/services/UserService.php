<?php
namespace App\Services;
use Illuminate\Support\Facades\DB;

class UserService {

public function getUserGenderStats(Request $request){
    $statusFilters = $request->input('status', []);
    $query = DB::table('users')
        ->leftJoin('profil', 'users.user_id', '=', 'profil.user_id')
        ->select(DB::raw("
            CASE
                WHEN profil.gender = 'male' THEN 'Male'
                WHEN profil.gender = 'female' THEN 'Female'
                ELSE 'Non-specified'
            END AS gender,
            COUNT(*) AS count
        "))
        ->whereIn('users.status', $statusFilters)
        ->where(function ($query) {
            $query->where(function ($query) {
                $query->where('users.status', 'Active')
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('')
                            ->whereColumn('packages.user_id', 'users.user_id');
                    });
            })->orWhere(function ($query) {
                $query->where('users.status', '<>', 'Deleted')
                    ->orWhereNull('users.deleted_at');
            });
        })
        ->groupBy('gender')
        ->get();

    return response()->json($query);
}
}