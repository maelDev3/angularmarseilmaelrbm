<?php
namespace App\Services;
use Illuminate\Support\Facades\DB;

class FileSysteme {

  
  public static function  addImage($path,$image,$imageName)
  {
      $filename= strtolower(uniqid($imageName).'.'.$image->getClientOriginalExtension());
      str_replace(' ','-',$filename);
      return basename($image->move($path,$filename));
  }

  public static function updateImage($path,$image,$oldImage,$imageName)
  {
    $filename= strtolower(uniqid($imageName).'.'.$image->getClientOriginalExtension());
    str_replace(' ','-',$filename);
    $move_image = basename($image->move($path,$filename));
    //Delete old image
    $image_path=public_path().'/'.$path.'/'.basename($oldImage);
    if(file_exists($image_path)){
        unlink($image_path);
    }
    //end delete image
    return $move_image;

  }
}