<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageAction extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function document(){
      return $this->hasOne(Document::class);
    }
    public function package(){
      return $this->belongsTo(Package::class);
    }
}
