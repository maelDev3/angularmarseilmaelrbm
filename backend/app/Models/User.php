<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    protected $appends = [
        'str_name',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function profile()
    {
        return $this->hasOne(Profil::class, 'user_id');
    }

    public function getStrNameAttribute()
    {
        return  ($this->attributes['name'] ? strtoUpper($this->attributes['name'][0]):'').''.($this->attributes['firstname'] ? strtoUpper($this->attributes['firstname'][0]) : '');
    }
    public function PhotosProfil()
    {
        return $this->hasMany(PhotosProfil::class, 'user_id');
    }
    public function packages()
    {
       return $this->hasMany(Package::class);
    }

     //Add the below function
     public function messages()
     {
         return $this->hasMany(Message::class);
     }  


}
